# Synopsis

Parallèlement au couple "Shell - SSH" pour administrer une machine linux à distance, sous Windows, on trouve l'équivalent "PowerShell - WinRM".  
L'administration à distance via PowerShell, appelée également PowerShell remoting, s'effectue par une connexion via WinRM sur l'hôte distant.
WinRM est l'implémentation Microsoft du standard WS-Management, basé sur SOAP.
Il ne nécessite qu'un port : 5985 pour HTTP, ou 5986 pour HTTPS.

# En powershell sur la machine à administrer du domaine avec un droit administrateur local

Etat du service :

    get-service winrm

Mise en place du service :

    enable-psremoting
    
# En powershell sur une machine du domaine avec un compte administrateur du domaine

Ouvrir un powershell sur la machine distante hote1.monDomaine.fr :

    enter-pssession hote1.monDomaine.fr
