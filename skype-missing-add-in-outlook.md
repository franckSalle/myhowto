# Fixing the missing Skype Meeting Add-in for Outlook  

This problem has been popping up for our users the last year: Whenever a user would start Outlook (after reboot or simply restarting Outlook) the Skype Meeting Add-in would be missing from the ribbon and had to be manually enabled to show up again.  


## Run Outlook as administrator.   
## Remove the Skype Meeting Add-in from the list. 
Navigate to File -> Options -> Add-Ins -> COM Add-ins.  
## Re-add the Meeting Add-in from the same menu.  
Path to the add-in is dependant on your Office version. The add-in itself is named UcAddin.dll  
If you are running x86 version then the path is %programfiles(x86)%\Microsoft Office\root\Office 16 for Outlook 2016 or [..]\Office 15 for Outlook 2013
If your are running x64 version the path is %programfiles%\Microsoft Office\root\Office 16 for Outlook 2016 or [..]\Office 15 for Outlook 2013   
This may differ if you are running Click-to-run Office version  
## Now close Outlook and restart in your regular user context.  