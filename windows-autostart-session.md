# Ouvrir une session et un programme au démarrage sous Windows
On crée un utilisateur local sur la machine, par exemple MonUser avec le mot de passe MonPassword sur la machine MonDomaine.  
On désactive l’action CTRL+ALT+SUPPR, sous panneau de configuration/comptes des utilisateurs/options avancées, décocher ouverture de session sécurisée.  
On édite le registre. On appuie simultanément sur les touches Windows et R. La boîte de dialogue Exécuter s’affiche. On tape regedit.  
On modifie la clé de registre HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon, pour ajouter les valeurs suivantes.  

    AutoAdminLogin      REG_SZ  1
    DefaultDomainName   REG_SZ  MonDomaine
    DefaultUserName     REG_SZ  MonUser
    DefaultPassword     REG_SZ  MonPassword
    
## Pour Windows 7  
Pour lancer un programme automatiquement, il suffit de placer un raccourci vers ce programme sous C:\ProgramData\Microsoft\Windows\Start Menu\Programs\StartUp .  

## Pour Windows 10  
On appuie simultanément sur les touches Windows et R. La boîte de dialogue Exécuter s’affiche. On tape shell:startup.

Le dossier c:\utilisateurs\votrenomdutilisateur\AppData\Roaming\Microsoft\Windows\Menu Démarrer\Programmes\Démarrage s’ouvre dans l’explorateur de fichiers. C’est dans ce dossier que l’on peut créer le raccourci vers l’application que l’on veut lancer au démarrage de Windows.
