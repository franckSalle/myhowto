# Synopsis

# Renommer une machine

    rename-computer -newname "nouveanNom"
    
# Ajouter une machine à un domaine

    add-computer -domainname mondomaine.fr -credential administrateur@mondomaine.fr
    
# Ajouter un utilisateur du domaine au groupe local des administrateurs

    $groupe = [adsi]"WinNT://localhost/Administrateurs,group"
    $groupe.add("WinNT://mondomaine.fr/monUtilisateur,user")

# Enlever un utilisateur du domaine au groupe local des administrateurs

    $groupe = [adsi]"WinNT://localhost/Administrateurs,group"
    $groupe.remove("WinNT://mondomaine.fr/monUtilisateur,user")
    
# Lister le top 10 des utilisateurs du CPU

    $fichier = "C:\Users\Administrateur\log\list-top-cpu.txt"
    $owners = @{}
    gwmi win32_process |% {$owners[$_.handle] = $_.getowner().user}
    get-process | sort-object CPU -descending | select -first 10 | select cpu,processname,Id,@{l="Owner";e={$owners[$_.id.tostring()]}} > $fichier
    # A partir de powershell 4, une seule commande suffit :
    # get-process -includeusername | sort-object CPU -descending | select -first 10 > $fichier
    
# Supprimer tous les fichiers de plus de 15 jours du répertoire c:\tmp

    forfiles -p c:\tmp -d -15 -c "cmd /c del @path"