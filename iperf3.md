# Synopsis

iPerf est un logiciel de mesure active de la bande passante disponible pour les réseaux IP.
Avec le nom générique de iPerf, nous nous référons spécifiquement à iPerf3, publié sous licence BSD et téléchargeable gratuitement à partir de sa page officielle Github : https://github.com/esnet/iperf .
iPerf, grâce à la prise en charge de tous les systèmes d’exploitation les plus répandus (y compris les systèmes mobiles tels qu’Android et iOS) et à la possibilité de régler de nombreux paramètres dans l’analyse de la performance réseau pour différents protocoles, est certainement l’un des outils de référence pour le diagnostic réseau.

# Côté serveur, écoute sur le port 5201 par défaut

    iperf3 -s
    
# Côté client

    iperf3 -c IP_SERVEUR
    # -R, pour exécuter le test dans le sens opposé (le serveur envoie les paquets et le client les reçoit).
    