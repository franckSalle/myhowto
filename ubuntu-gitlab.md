# Install GitLab 
    sudo apt install git -y
    mkdir ~/Projets
    cd Projets
    git init

# Clone distant depot
    git clone https://<dépot>

# Update distant depot from local
    cd Projets/monProjet
    git add *
    git commit -m "update from local"
    git push origin master

# Update local depot from distant
    cd Projets/monProjet
    git pull