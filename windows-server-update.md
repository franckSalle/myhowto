Pour une mise à jour automatique avec redémarrage
Exécuter gpedit.msc

    > configuration ordinateur
    > modèles d’administration
    > composants windows
    > windows update
    > configuration du service de MAJ automatique.
    
    Mettre Activé option 4 : installation automatique de windows et des produits microsoft tous les dimanches à 12h.