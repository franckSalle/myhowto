# Introduction
PsPing implements Ping functionality, TCP ping, latency and bandwidth measurement between 2 hosts.  
You can download it at https://live.sysinternals.com  
# Server IP:PORT (192.168.1.16:8000 for example)

    psping -s IP:PORT
# Client
To test server with 10000 packets of 8K bytes.  

    psping -b -l 8k -n 10000 IP:PORT