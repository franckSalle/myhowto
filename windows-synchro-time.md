In an Active Directory domain, the time of some Windows machines is out of sync. The script running on the machine in error corrects this problem. 

    net time /set /domain:monDomaine /y