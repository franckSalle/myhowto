Many administrators are not happy with two services in Server 2016. This services are OneSyncSvc (synchronizes mail, contacts, calendar and various other user data) and Download Maps Manager (for application access to downloaded maps) and are really not critical on server OS.
This is the reason why, you can disable them in almost all cases without any deep research. Anyway, when you try to disable OneSyncSvc from services console, you will receive an error, as this service cannot be disabled. At this point, use a trick and disable both services form elevated command prompt using commands:

    sc config “OneSyncSvc” start= disabled 
    sc config “MapsBroker” start= disabled
    
Of course, before you run this commands, services have to be stopped, otherwise you will receive an error. You can do this also from command prompt:

    sc stop “OneSyncSvc”
    sc stop “MapsBroker”
    
This two simple commands will put services into startup type “disabled” and errors in Server Manager that are related to non-running services will disappear. Problem solved.